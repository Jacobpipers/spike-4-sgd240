// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/GameStateBase.h"
#include "MyGameStateBase.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class PLATFORM_API AMyGameStateBase : public AGameStateBase
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintCallable, Category = "Score")
	int32 GetScore();
	UFUNCTION(BlueprintCallable, Category = "Score")
	int32 SetScore(int32 value);

private:

	int32 currentScore;
	
};