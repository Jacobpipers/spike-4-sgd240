// Fill out your copyright notice in the Description page of Project Settings.

#include "platform.h"
#include "MyGameStateBase.h"

int32 AMyGameStateBase::SetScore(int32 value)
{
	currentScore += value;

	return currentScore;
}

int32 AMyGameStateBase::GetScore()
{
	return currentScore;
}