# Spike Report

## SPIKE 4 - UNREAL ENGINE GAMEPLAY FRAMEWORK

### Introduction

We’ve just done some tutorials on how to use C++ and Blueprints together, but now we need to look across the entirety of how Unreal Engine works to understand it.
Let’s build a small project in Unreal that explores each of the areas, and write a report that explains what is the purpose of (and relationships between) each of the following:


### Goals

1.	A small Unreal Engine project, built from scratch (can use content from any project from the Learn Tab in the Epic Games Launcher).
	1.	Any small game which you like, manage the scope yourself.
	2.	I would recommend finding a classic Arcade game that you want to copy.
2.	The project must include the following custom classes (i.e. child classes):
	1.	Game Mode (using Blueprint or C++)
		1.	Sets the game to use the Pawn, Controller and GameState below
2.	A Pawn or Character (C++)
	1.	Game Logic for any playable/controllable Actors goes here
	2.	There must be at least one UFUNCTION which the Controller can call
	3.	There must be at least one UPROPERTY which the Controller can read or write to
3.	A Player Controller (Blueprints or C++)
	1.	This must use the C++ functions and variables from the Pawn/Character
	2.	Control logic goes here (i.e. how to convert Input for the Pawn to use)
	3.	It must support at LEAST: Mouse and Keyboard
	4.	Should also support one of:
		1.	Game Pad
		2.	Virtual Reality Controllers
4.	A GameState (C++)
	1.	It should track a Score of some kind (or Timer, or similar)



### Personnel

In this section, list the primary author of the spike report, as well as any personnel who assisted in completing the work.

* Primary - Jacob Pipers
* Secondary - N/A

### Technologies, Tools, and Resources used

Provide resource information for team members who wish to learn additional information from this spike.

* https://docs.unrealengine.com/latest/INT/Gameplay/Framework/GameMode/index.html
* https://docs.unrealengine.com/latest/INT/Gameplay/Framework/Pawn/index.html
* https://docs.unrealengine.com/latest/INT/Gameplay/Framework/Pawn/Character/index.html
* https://docs.unrealengine.com/latest/INT/Gameplay/Framework/Controller/index.html 
* https://docs.unrealengine.com/latest/INT/Gameplay/Framework/Camera/index.html 
* https://docs.unrealengine.com/latest/INT/Programming/Introduction/index.html
* https://docs.unrealengine.com/latest/INT/Videos/PLZlv_N0_O1gYup-gvJtMsgJqnEB_dGiM4/mSRov77hNR4/
* Visual Studio
* Unreal Engine


### Tasks Undertaken

Show only the key tasks that will help the developer/reader understand what is required.

Create a C++ project Using the Side Scroller Template

1. Create a [Game Mode](https://docs.unrealengine.com/latest/INT/Gameplay/Framework/GameMode/index.html) using this [Guide](https://docs.unrealengine.com/latest/INT/Gameplay/HowTo/SettingUpAGameMode/Blueprints/index.html). The Game Mode will be used to control what character/pawn is spawned in the world and setup other default actions

2. Create a  [Character](https://docs.unrealengine.com/latest/INT/Gameplay/Framework/Pawn/Character/index.html)
	* Add a public function called MoveForward with [UFUNCTION](https://docs.unrealengine.com/latest/INT/Programming/UnrealArchitecture/Reference/Functions/)
	* Add a public Function OnStartJump with UFUNCTION
	
	* This Code will move the character along the X Axis.
	
			```c++
				void APlatformCharacter::MoveForward(float Value)
				{

				// Find out which way is "forward" and record that the player wants to move that way.
				FVector Direction;

				if (Controller)
				{
					Direction = FRotationMatrix(Controller->GetControlRotation()).GetScaledAxis(EAxis::X);
					AddMovementInput(Direction, Value);
				}

				}
			```
	
	*  Thell the Player to jump
			void APlatformCharacter::OnStartJump()
			{
				bPressedJump = true;
			}

			void APlatformCharacter::OnStopJump()
			{
				bPressedJump = false;
			}

3. Create a [Player Controller](https://docs.unrealengine.com/latest/INT/Gameplay/Framework/Controller/PlayerController/index.html)

	* Setup Input in the Playrer Controller:
		
			void AMainPlayerController::SetupInputComponent()
			{
				Super::SetupInputComponent();
		
				InputComponent->BindAxis("MoveForward", this, &AMainPlayerController::MoveCharacter);
				InputComponent->BindAction("Jump", IE_Pressed, this, &AMainPlayerController::OnStartJump);
				InputComponent->BindAction("Jump", IE_Released, this, &AMainPlayerController::OnStopJump);
			}

	* Declare these functions in the player controller. 

			void AMainPlayerController::MoveCharacter(float Value)
			{
				if (ControlledCharacter)
				{
					AMainPlayerController::GetPlatformCharacter()->MoveForward(Value);
				}
			}

			void AMainPlayerController::OnStartJump()
			{
				if (ControlledCharacter)
				{
					AMainPlayerController::GetPlatformCharacter()->OnStartJump();
				}
			}

			void AMainPlayerController::OnStopJump()
			{
				if (ControlledCharacter)
				{
					AMainPlayerController::GetPlatformCharacter()->OnStopJump();
				}
			}

	* In Project Settings Add Mapping as seen in picture below 
		
		![](SpikeImages/InputSettings.png)


5. Add a score in [Game State](https://docs.unrealengine.com/latest/INT/Gameplay/Framework/GameMode/index.html#gamestate)

6. Assign The player controller and the GameState we created to the World Settings as well set The Default Pawn to The Character we made.

### What we found out

We Found out how to Create and use GameMode, GameState and PLayer states.

We Found out how to create a pawn and assign controls

We Found out how To use Cameras in the unreal engine.

A good place to track a players Stats is in the Game or Player state classes.

The benifients of Using a Player Controller in a small project such as a platformer where the player is likely to only ever control one character or pawn can be hard to fully show it's use and how it can be used.

Ussing the Player Controller we are able to map Inputs as minimal as possible allowing for instance to check what is being controlled such as a vechiel.

### [Optional] Open Issues/Risks

List out the issues and risks that you have been unable to resolve at the end of the spike. You may have uncovered a whole range of new risks as well.

### [Optional] Recommendations

You may state that another spike is required to resolve new issues identified (or) indicate that this spike has increased the team�s confidence in XYZ and move on.